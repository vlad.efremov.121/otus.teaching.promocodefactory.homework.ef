﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IPromoCodeRepository
    {
        Task<IEnumerable<PromoCode>> GetAllAsync();

        Task CreateAsync(PromoCode entity);
    }
}
