﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference
    {
        public Guid CustomerId { get; set; }
        public Guid PreferenceId { get; set; }
        public virtual Customer Customers { get; set; }
        public virtual Preference Preferences { get; set; }
    }
}
