﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPromoCode
    {
        public Guid CustomerId { get; set; }
        public Guid PromoCodeId { get; set; }

        public virtual Customer Customers { get; set; }
        public virtual PromoCode PromoCodes { get; set; }
    }
}
