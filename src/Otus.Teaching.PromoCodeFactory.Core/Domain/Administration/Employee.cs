﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }
        public Guid RoleId { get; set; }


        public int AppliedPromocodesCount => PromoCodes.Count;
        public virtual Role Role { get; set; }
        public virtual ICollection<PromoCode> PromoCodes { get; set; }
    }
}