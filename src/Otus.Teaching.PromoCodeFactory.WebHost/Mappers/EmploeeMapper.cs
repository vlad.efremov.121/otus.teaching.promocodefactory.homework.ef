﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class EmploeeMapper
    {
        public static EmployeeResponse FromDto(this Employee employee)
        {
            return new EmployeeResponse
            {
                Id = employee.Id,
                Email = employee.Email,
                Role = new RoleItemResponse()
                {
                    Id = employee.RoleId,
                    Name = employee.Role.Name,
                    Description = employee.Role.Description
                },
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

        }
        public static EmployeeShortResponse ShortFromDto(this Employee employee)
        {
            return new EmployeeShortResponse
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName,
            };

        }
    }
}
