﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class PromoCodeMapper
    {
        public static PromoCodeShortResponse ShortFromDto (this PromoCode promoCode)
        {
            return new PromoCodeShortResponse
            {
                Id = promoCode.Id,
                Code = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                BeginDate = promoCode.BeginDate.ToString(),
                EndDate = promoCode.EndDate.ToString(),
                PartnerName = promoCode.PartnerName
            };
        }

        public static PromoCode ToDto (this GivePromoCodeRequest request)
        {
            return new PromoCode
            {
                Id = Guid.NewGuid(),
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                Code = request.Code,
                Preference = new Preference { Name = request.Preference }
            };
        }
    }
}
