﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class CustomerMapper
    {
        public static Customer ToDto(this CreateOrEditCustomerRequest request, Guid? id = null)
        {
            var _id = id.HasValue ? id.Value : Guid.NewGuid();
            return new Customer 
            {
                Id = _id,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                CustomerPreferences = request.PreferenceIds
                .Select(x => new CustomerPreference { PreferenceId = x, CustomerId = _id })
                .ToList(),
            };
        }

        public static CustomerResponse FromDto(this Customer customer)
        {
            return new CustomerResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Prefernces = customer.CustomerPreferences
                .Select(x => x.Preferences)
                .Select(x => x.FromDto())
                .ToList(),
                PromoCodes = customer.CustomerPromoCodes
                .Select(x => x.PromoCodes)
                .Select(x => x.ShortFromDto())
                .ToList(),
            };
        }

        public static CustomerShortResponse ShortFromDto(this Customer customer)
        {
            return new CustomerShortResponse
            {
               Id = customer.Id,
               Email = customer.Email,
               FirstName = customer.FirstName,
               LastName = customer.LastName,
            };
        }

    }
}
