﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class PreferenceMapper
    {
        public static PreferenceResponse FromDto (this Preference preference)
        {
            return new PreferenceResponse
            { 
                Id = preference.Id,
                Name = preference.Name
            };

        }
    }
}
