﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Constants;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromoCodeRepositories : IPromoCodeRepository
    {
        private readonly DataBaseContext context;

        public PromoCodeRepositories(DataBaseContext context)
        {
            this.context = context;
        }

        public async Task CreateAsync(PromoCode entity)
        {
            var preference = await context.Preferences
                .FirstOrDefaultAsync(x => x.Name.Equals(entity.Preference.Name));
            if (preference == null) throw new Exception("Preferece by name does not exist");
            
            var partner = await context.Employees
                .FirstOrDefaultAsync(x => x.FullName.Equals(entity.PartnerName));
            if (partner == null) throw new Exception("Employee by name does not exist");
            if(!partner.Role.Id.Equals(Constants.PartnerRoleId)) throw new Exception("This employee is not PartnerManager");

            var customerIds =  context.CustomerPreferences
                .Where(x => x.PreferenceId.Equals(preference.Id)).Select(x => x.CustomerId);
            var newCustomerPromoCodes = customerIds.Select(x => new CustomerPromoCode
            { 
                CustomerId = x, 
                PromoCodeId = entity.Id 
            });

            var transaction = await context.Database.BeginTransactionAsync();
            await context.PromoCodes.AddAsync(new PromoCode
            {
                Id = entity.Id,
                Code = entity.Code,
                ServiceInfo = entity.ServiceInfo,
                PartnerName = entity.PartnerName,
                BeginDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(30),
                PartnerId = partner.Id,
                PreferenceId = preference.Id
            });
            await context.CustomerPromoCodes.AddRangeAsync(newCustomerPromoCodes);
            await context.SaveChangesAsync();
            await transaction.CommitAsync();
        }

        public async Task<IEnumerable<PromoCode>> GetAllAsync()
        {
           return await context.PromoCodes.ToListAsync();
        }
    }
}
