﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepositories<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DataBaseContext context;

        public EfRepositories(DataBaseContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var recodrs = await context.DbSet<T>().ToListAsync();
            return recodrs;
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            var entity = GetIfExist(id);
            return entity;
        }

        public async Task CreateAsync(T entity) 
        {
            await context.DbSet<T>().AddAsync(entity);
            await context.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            await GetIfExist(entity.Id);
            context.DbSet<T>().Update(entity);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await GetIfExist(id);
            context.DbSet<T>().Remove(entity);
            await context.SaveChangesAsync();
        }

        private async Task<T> GetIfExist(Guid id)
        {
            var entity = await context.DbSet<T>().FindAsync(id);
            if(entity == null) 
                throw new Exception("Entity by id not found");
            return entity;
        }
    }
}
