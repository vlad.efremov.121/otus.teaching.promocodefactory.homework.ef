﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class SeedCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("eb869c86-453e-4fb7-8b14-216a75ca0f1b"), "email@email.ru", "Иван", "Иванов" });

            migrationBuilder.InsertData(
                table: "PromoCodes",
                columns: new[] { "Id", "BeginDate", "Code", "EndDate", "PartnerId", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("d12f9cfa-1bb4-45d0-b5b4-15bc39557331"), new DateTime(2023, 3, 18, 11, 25, 31, 958, DateTimeKind.Utc).AddTicks(2839), "PROMO123", new DateTime(2023, 4, 17, 11, 25, 31, 958, DateTimeKind.Utc).AddTicks(2973), new Guid("f766e2bf-340a-46ea-bff3-f1700b435895"), "Иван Сергеев", new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), "seeds promoCode" });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("eb869c86-453e-4fb7-8b14-216a75ca0f1b"), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") });

            migrationBuilder.InsertData(
                table: "CustomerPromoCodes",
                columns: new[] { "CustomerId", "PromoCodeId" },
                values: new object[] { new Guid("eb869c86-453e-4fb7-8b14-216a75ca0f1b"), new Guid("d12f9cfa-1bb4-45d0-b5b4-15bc39557331") });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumns: new[] { "CustomerId", "PreferenceId" },
                keyValues: new object[] { new Guid("eb869c86-453e-4fb7-8b14-216a75ca0f1b"), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") });

            migrationBuilder.DeleteData(
                table: "CustomerPromoCodes",
                keyColumns: new[] { "CustomerId", "PromoCodeId" },
                keyValues: new object[] { new Guid("eb869c86-453e-4fb7-8b14-216a75ca0f1b"), new Guid("d12f9cfa-1bb4-45d0-b5b4-15bc39557331") });

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("eb869c86-453e-4fb7-8b14-216a75ca0f1b"));

            migrationBuilder.DeleteData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("d12f9cfa-1bb4-45d0-b5b4-15bc39557331"));
        }
    }
}
