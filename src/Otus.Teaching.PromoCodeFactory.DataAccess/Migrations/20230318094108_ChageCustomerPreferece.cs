﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class ChageCustomerPreferece : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreference_Customer_CustomerId",
                table: "CustomerPreference");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreference_Preferences_PreferenceId",
                table: "CustomerPreference");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPromoCodes_Customer_CustomerId",
                table: "CustomerPromoCodes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CustomerPreference",
                table: "CustomerPreference");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Customer",
                table: "Customer");

            migrationBuilder.RenameTable(
                name: "CustomerPreference",
                newName: "CustomerPreferences");

            migrationBuilder.RenameTable(
                name: "Customer",
                newName: "Customers");

            migrationBuilder.RenameIndex(
                name: "IX_CustomerPreference_CustomerId",
                table: "CustomerPreferences",
                newName: "IX_CustomerPreferences_CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_Customer_Email",
                table: "Customers",
                newName: "IX_Customers_Email");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CustomerPreferences",
                table: "CustomerPreferences",
                columns: new[] { "PreferenceId", "CustomerId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Customers",
                table: "Customers",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreferences_Customers_CustomerId",
                table: "CustomerPreferences",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreferences_Preferences_PreferenceId",
                table: "CustomerPreferences",
                column: "PreferenceId",
                principalTable: "Preferences",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPromoCodes_Customers_CustomerId",
                table: "CustomerPromoCodes",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreferences_Customers_CustomerId",
                table: "CustomerPreferences");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreferences_Preferences_PreferenceId",
                table: "CustomerPreferences");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPromoCodes_Customers_CustomerId",
                table: "CustomerPromoCodes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Customers",
                table: "Customers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CustomerPreferences",
                table: "CustomerPreferences");

            migrationBuilder.RenameTable(
                name: "Customers",
                newName: "Customer");

            migrationBuilder.RenameTable(
                name: "CustomerPreferences",
                newName: "CustomerPreference");

            migrationBuilder.RenameIndex(
                name: "IX_Customers_Email",
                table: "Customer",
                newName: "IX_Customer_Email");

            migrationBuilder.RenameIndex(
                name: "IX_CustomerPreferences_CustomerId",
                table: "CustomerPreference",
                newName: "IX_CustomerPreference_CustomerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Customer",
                table: "Customer",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CustomerPreference",
                table: "CustomerPreference",
                columns: new[] { "PreferenceId", "CustomerId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreference_Customer_CustomerId",
                table: "CustomerPreference",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreference_Preferences_PreferenceId",
                table: "CustomerPreference",
                column: "PreferenceId",
                principalTable: "Preferences",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPromoCodes_Customer_CustomerId",
                table: "CustomerPromoCodes",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
