﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Constants;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class DataSeed
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {

            #region Roles
            var admin = new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            };

            var partnerManager = new Role()
            {
                Id = Constants.PartnerRoleId,
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            };

            modelBuilder.Entity<Role>().HasData(admin, partnerManager);
            #endregion

            #region Employee
            var employeeAdmin = new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = admin.Id
            };
            var employeePartner = new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = partnerManager.Id
            };

            modelBuilder.Entity<Employee>().HasData(employeeAdmin, employeePartner);
            #endregion

            #region Preference
            var theater = new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            };
            var family = new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            };
            var children = new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            };

            modelBuilder.Entity<Preference>().HasData(theater, family, children);
            #endregion

            #region PromoCode
            var promo = new PromoCode
            {
                Id = Guid.Parse("D12F9CFA-1BB4-45D0-B5B4-15BC39557331"),
                Code = "PROMO123",
                BeginDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(30),
                PartnerId = employeePartner.Id,
                PartnerName = employeePartner.FullName,
                PreferenceId = theater.Id,
                ServiceInfo = "seeds promoCode"
            };

            modelBuilder.Entity<PromoCode>().HasData(promo);
            #endregion

            #region Customer
                var customer = new Customer
                {
                    Id = Guid.Parse("EB869C86-453E-4FB7-8B14-216A75CA0F1B"),
                    FirstName = "Иван",
                    LastName = "Иванов",
                    Email = "email@email.ru"
                };

                modelBuilder.Entity<Customer>().HasData(customer);
            #endregion

            #region CustomerPreference
                modelBuilder.Entity<CustomerPreference>().HasData(new CustomerPreference
                {
                    CustomerId = customer.Id,
                    PreferenceId = theater.Id,
                });
            #endregion

            #region CustomerPromoCode
                modelBuilder.Entity<CustomerPromoCode>().HasData(new CustomerPromoCode
                {
                    CustomerId = customer.Id,
                    PromoCodeId = promo.Id,
                });
            #endregion
        }
    }
}