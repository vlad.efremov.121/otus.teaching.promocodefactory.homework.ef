﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Cofigurations
{
    public class CustomerPromoCodeConfiguration : IEntityTypeConfiguration<CustomerPromoCode>
    {
        public void Configure(EntityTypeBuilder<CustomerPromoCode> builder)
        {
            builder.HasKey(x => new { x.PromoCodeId, x.CustomerId });

            builder.HasOne(x => x.PromoCodes)
                .WithMany(x => x.CustomerPromoCodes)
                .HasForeignKey(x => x.PromoCodeId);

            builder.HasOne(x=>x.Customers)
                .WithMany(x=>x.CustomerPromoCodes)
                .HasForeignKey(x=>x.CustomerId);
        }
    }
}
