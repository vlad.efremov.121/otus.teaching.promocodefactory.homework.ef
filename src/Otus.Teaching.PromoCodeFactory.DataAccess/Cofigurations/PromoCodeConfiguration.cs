﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Cofigurations
{
    public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.Code).IsUnique();

            builder.HasOne(x => x.PartnerManager)
                .WithMany(x => x.PromoCodes)
                .HasForeignKey(x => x.PartnerId);

            builder.HasOne(x => x.Preference)
                .WithMany(x => x.PromoCodes)
                .HasForeignKey(x => x.PreferenceId);

        }
    }
}
