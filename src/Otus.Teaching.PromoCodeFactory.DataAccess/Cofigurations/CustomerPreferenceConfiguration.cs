﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Cofigurations
{
    public class CustomerPreferenceConfiguration : IEntityTypeConfiguration<CustomerPreference>
    {
        public void Configure(EntityTypeBuilder<CustomerPreference> builder)
        {
            builder.HasKey(x => new {x.PreferenceId, x.CustomerId});

            builder.HasOne(x => x.Customers)
                 .WithMany(x => x.CustomerPreferences)
                 .HasForeignKey(x => x.CustomerId);
            
            builder.HasOne(x=> x.Preferences)
                .WithMany(x=>x.CustomerPreferences)
                .HasForeignKey(x=>x.PreferenceId);
                
        }
    }
}
